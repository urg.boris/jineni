﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    public class ItemVisual: MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IPointerEnterHandler, IDropHandler
    {
        public ItemCore item;
        public Image image;
        public RectTransform rectTransform;

        Vector2 startingPos;

        private void Awake()
        {
            image = GetComponent<Image>();
            rectTransform = GetComponent<RectTransform>();
            Assert.IsNotNull(image);
            Assert.IsNotNull(rectTransform);
        }


        //===============CLICKED MOUSE DRAG

        public void OnDrop(PointerEventData data)
        {
            if (data.pointerDrag != null)
            {
                Debug.Log("Dropped object was: " + data.pointerDrag);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                InventoryManager.Instance.StartDraggingItem(this);
            }
        }
        
        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                InventoryManager.Instance.TestDropTarget(this);
                //!!must call stop dragging, TestDropTarget might end prematurely
                InventoryManager.Instance.StopDraggingItem();
            } else if (eventData.button == PointerEventData.InputButton.Middle)
            {
                //!! pro slona
                Debug.Log("Middle click");
            } else if (eventData.button == PointerEventData.InputButton.Right)
            {
                InventoryManager.Instance.StopDraggingItem();
                if (GameManager.Instance.CanItemBeExamined())
                {
                    DialogManager.Instance.StartExamination(item);
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            Cursor.SetCursor(GameManager.Instance.cursor, Vector2.zero, CursorMode.Auto);
        }

        void OnMouseExit()
        {
            ChangeToDefaultCursor();
        }

        void ChangeToDefaultCursor()
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

        public void OnPointerClick(PointerEventData eventData)
        {

        }
    }
}


