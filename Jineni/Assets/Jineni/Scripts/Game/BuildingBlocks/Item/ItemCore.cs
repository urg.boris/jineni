﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem;
using UnityEditor;
using UnityEngine.Assertions;
using Jineni.DialogSystem.Graph;
using Jineni.Game.BuildingBlocks.Internals;
using Jineni.Localization;
using Jineni.Utils;

namespace Jineni.Game.BuildingBlocks
{
    [CreateAssetMenu(menuName = "Jineni/new item", order = 3)]
    public class ItemCore : EntryPointLinkable, IEditorNamable
    {
        public string editorName = "name for editor";
        public Translation[] names = new Translation[1];
        public int currentNameIndex = 0;
        public EntryPoint currentExamineEntryPoint;

        //public EntryPoint entryPoint;
        public Sprite icon;


        private void OnEnable()
        {
            //!!TODO oncreate haze chybu, asi vlastni checker
            //Assert.IsNotNull(icon);
            //Assert.IsNotNull(entryPoint);
        }



        public string GetEditorName()
        {
            return "I" + uniqueId + " - " + editorName;
        }

        public string GetCurrentName(Lang lang)
        {
            if(!ArrayHelper.IsIndexInArrayRange(currentNameIndex, name.Length))
            {
                Siren.EditorWarn("not in index range");
                return names[0].Get(lang);
            }
            return names[currentNameIndex].Get(lang);
        }

        public void SetNameIndex(int index)
        {
            if (!ArrayHelper.IsIndexInArrayRange(index, names.Length))
            {
                Assert.IsTrue(false, "cant change name index; out of boundaries; i=" + index + "; count=" + names.Length);
                return;
            }
            currentNameIndex = index;
        }

        public void ChangeExamineEP(EntryPoint entryPoint)
        {
            currentExamineEntryPoint = entryPoint;
        }

        public override void ResetToDefaultState()
        {
            currentNameIndex = 0;
        }
    }
}
