﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    public class InventoryManager : MonoBehaviour, IStateResetable
    {

        const string CHARACTER_TAG = "Character";
        //public const string droptargetItem = "DropTargetItem";
        //public const string droptargetCharacter = "DropTargetCharacter";

        public GameManager gameManager;

        public Inventory inventory;
        public Canvas dynamicCanvas;
        public RectTransform inventoryRect;
        public GameObject itemsContainer;
        public GameObject itemVisualPrefab;
        public GridLayoutGroup itemsGrid;
        public List<ItemVisual> dropTargets;
        public List<BuildingBlocks.CharacterController> characterDropTargets;

        public bool isOpened = false;
        

        public bool itemIsDragging = false;
        private ItemVisual currentItemVisual;
        private Image itemVisualDragged;
        private Vector3 startDraggingMousePos;
        private Vector3 startDraggingPos;

        static InventoryManager _instance = null;
        public static InventoryManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<InventoryManager>();
                }
                //!! must remain null if doesnt exists, checker will create it
                return _instance;
            }
        }

        private void Awake()
        {
            Assert.IsNotNull(inventory);
            Assert.IsNotNull(dynamicCanvas);
            Assert.IsNotNull(itemsContainer);
            Assert.IsNotNull(itemVisualPrefab);
            Assert.IsNotNull(itemsGrid);


            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
            //!!is part of GameManager
            //DontDestroyOnLoad(gameObject);

            Init();
        }

        private void Init()
        {
            SetVisibility(false);
            inventory.inventoryChanged = true;
        }


        public void Open()
        {
            if(isOpened)
            {
                return;
            }
            if (!gameManager.CanInventoryBeOpened())
            {
                return;
            }

            gameManager.ChangeCursorToDefault();
            SetVisibility(true);
            ShowItems();

        }

        public void Close()
        {
            if (isOpened)
            {
                gameManager.ChangeCursorToDefault();
                SetVisibility(false);
                itemIsDragging = false;
            }
        }

        public void SetVisibility(bool opended)
        {
            isOpened = opended;
            dynamicCanvas.gameObject.SetActive(isOpened);
        }

        public void Toogle()
        {
            if(isOpened)
            {
                Close();
            } else
            {
                Open();
            }
        }

        public void UpdateVisual()
        {
            inventory.inventoryChanged = true;
            ShowItems();
        }

        void ShowItems()
        {
            //Debug.Log("showing");
            //TODO pro testovani je lepsi necachovat
           // if(inventory.inventoryChanged)
           // {
                //Debug.Log("something changed");
                //!!activate laoutgoup, for corect drag drop handling
                itemsGrid.enabled = true;

                dropTargets = new List<ItemVisual>();
                int children = itemsContainer.transform.childCount;
                for (int i = 0; i < children; i++)
                {

                    //print(i+"For loop: " + itemsContainer.transform.GetChild(i).name);
                    Destroy(itemsContainer.transform.GetChild(i).gameObject);
                }

                //Debug.Log("inventoryChanged");
                for (int i = 0; i < inventory.items.Count; i++)
                {
                    GameObject itemUI = Instantiate(itemVisualPrefab, Vector3.zero, Quaternion.identity, itemsContainer.transform);
                    itemUI.name = "item-"+i+"-" + inventory.items[i].GetEditorName();

                    ItemVisual itemVisual = itemUI.GetComponent<ItemVisual>();
                //Debug.Log("dasdsa"+inventory.items[i]);
                //Debug.Log(itemVisual.image.sprite);
                    itemVisual.image.sprite = inventory.items[i].icon;
                    itemVisual.item = inventory.items[i];

                    dropTargets.Add(itemVisual);
                }
                inventory.inventoryChanged = false;
           /* } else
            {
                //!!everything already there
            }*/
            

        }

        public void AddItem(ItemCore item)
        {
            inventory.GainItem(item);
            //inventory.inventoryChanged = true;
        }

        public void RemoveItem(ItemCore item)
        {
            inventory.UseItem(item);
            //inventory.inventoryChanged = true;
        }

        public void StartDraggingItem(ItemVisual itemVisual)
        {
            if(!itemIsDragging)
            {
                itemIsDragging = true;
                //!!deactivate layoutgroup, protoze to koliduje s odlepenim currentItemVisual s gridu
                itemsGrid.enabled = false;
                currentItemVisual = itemVisual;
                itemVisual.transform.SetAsLastSibling();
                startDraggingPos = itemVisual.transform.position;
                startDraggingMousePos = Input.mousePosition;
            }
        }

        public void StopDraggingItem()
        {
            if(itemIsDragging)
            {
                itemIsDragging = false;
                currentItemVisual.transform.position = startDraggingPos;
                currentItemVisual = null;
            }
        }

        public void StartDraggingItemSticked(ItemVisual itemVisual)
        {
            if (!itemIsDragging)
            {
                itemIsDragging = true;
                //!!deactivate layoutgroup, protoze to koliduje s odlepenim currentItemVisual s gridu
                itemsGrid.enabled = false;
                currentItemVisual = itemVisual;
                itemVisual.transform.SetAsLastSibling();
                startDraggingPos = itemVisual.transform.position;
                startDraggingMousePos = Input.mousePosition;
            }
        }

        public void TestDropTarget(ItemVisual itemVisual)
        {
            //RectTransform skipItem = itemVisual.rectTransform;
            Vector2 mouse = Input.mousePosition;
            //!!inventory inventory drop
            if (RectTransformUtility.RectangleContainsScreenPoint(inventoryRect, mouse))
            {
                foreach (ItemVisual dropTarget in dropTargets)
                {
                    if (dropTarget == itemVisual)
                    {
                        continue;
                    }
                    if (RectTransformUtility.RectangleContainsScreenPoint(dropTarget.rectTransform, mouse))
                    {
                        //Debug.Log(dropTarget);
                        StartDialogFromCombinations(itemVisual.item, dropTarget.item);
                        return;
                    }
                }
            } else
            {
                Ray ray = gameManager.MainCamera.ScreenPointToRay(mouse);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
                if (hit)
                {
                    //Debug.Log(hit.collider.gameObject.name);
                    if (hit.collider.gameObject.CompareTag(CHARACTER_TAG))
                    {
                        BuildingBlocks.CharacterController characterController = hit.collider.gameObject.GetComponent<BuildingBlocks.CharacterController>();
                        Assert.IsNotNull(characterController);
                        StartDialogFromCombinations(itemVisual.item, characterController.character);
                        return;
                    }
                }
            }
        }

        public void StartDialogFromCombinations(ItemCore item1, ItemCore item2)
        {
            Debug.Log(item1.name + " " + item2.name);
            gameManager.dialogManager.StartDialog(inventory.itemCombinationsManager.GetEntryPointFromEntryBy(item1, item2));
            //Close();
        }

        public void StartDialogFromCombinations(ItemCore item1, CharacterCore item2)
        {
            gameManager.dialogManager.StartDialog(inventory.itemCombinationsManager.GetEntryPointFromEntryBy(item1, item2));
            Close();
        }



        private void Update()
        {
            if(itemIsDragging)
            {
                Vector3 currentPos = Input.mousePosition;
                Vector3 diff = currentPos - startDraggingMousePos;
                Vector3 pos = startDraggingPos + diff;
                currentItemVisual.transform.position = pos;
            }
        }

        public void ResetToDefaultState()
        {
            SetVisibility(false);
            inventory.inventoryChanged = true;
            inventory.ResetToDefaultState();
        }
    }
}