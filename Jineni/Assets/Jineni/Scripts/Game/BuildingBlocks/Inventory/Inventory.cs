﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem;
using Jineni.Game.BuildingBlocks.Internals;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{

    [CreateAssetMenu(fileName = "Inventory", menuName = "Jineni/Internal/Base/inventar", order = 21)]
    public class Inventory : ScriptableObject, IStateResetable
    {
        public List<ItemCore> items = new List<ItemCore>(50);
        public ItemCombinationsManager itemCombinationsManager;
        public int oddness;
        public bool inventoryChanged = true;

        private void Awake()
        {
            Assert.IsNotNull(itemCombinationsManager);
        }


        public void ResetToDefaultState()
        {
            items.Clear();
            oddness = 0;
            inventoryChanged = true;
        }

        public void IncrementOddness(int i)
        {
            oddness += i;
            if(oddness>=100)
            {
                oddness = 100;
                Debug.Log("reaching 100% oddness");
            }
        }

        public void DecrementOddness(int i)
        {
            oddness -= i;
            if (oddness <= 0)
            {
                oddness = 0;
                Debug.Log("reaching 0% oddness");
            }
        }

        public bool IsOddnessReached(int oddnessIntensity)
        {
            if(oddness >= oddnessIntensity)
            {
                return true;
            } else
            {
                return false;
            }
        }

        public bool IsInInventory(ItemCore item)
        {
            return items.Contains(item);
        }

        //TODO safefallback politika

        public void GainItem(ItemCore item)
        {
            if (!IsInInventory(item))
            {
                //TODO notice
                items.Add(item);
                InventoryManager.Instance.UpdateVisual();

            } else
            {
                Siren.EditorWarn("trying to AddItem item " + item.GetEditorName() + ", but its already in inventory");
            }
            
        }

        public void UseItem(ItemCore item)
        {
            //TODO unique jak to bude
            if(IsInInventory(item))
            {
                //TODO notice
                items.Remove(item);
                InventoryManager.Instance.UpdateVisual();
            } else
            {
                Siren.EditorWarn("trying to HasItem item " + item.GetEditorName() + ", but its not in inventory");
            }
        }

        public void LooseItem(ItemCore item)
        {
            if (IsInInventory(item))
            {
                //TODO notice
                items.Remove(item);
                InventoryManager.Instance.UpdateVisual();
            }
            else
            {
                //todo null check
                Siren.EditorWarn("trying to LooseItem item " + item.GetEditorName() + ", but its nor in inventory");
            }
        }

        public void HaveItem(ItemCore item)
        {
            if (!IsInInventory(item))
            {
                items.Add(item);
                InventoryManager.Instance.UpdateVisual();
            }
            else
            {
                Siren.EditorWarn("trying to HaveItem item " + item.GetEditorName() + ", but its already in inventory");
            }
        }

        public void NotHaveItem(ItemCore item)
        {
            //TODO unique jak to bude
            if (IsInInventory(item))
            {
                items.Remove(item);
                InventoryManager.Instance.UpdateVisual();
            }
            else
            {
                Siren.EditorWarn("trying to NotHaveItem item " + item.GetEditorName() + ", but its not in inventory");
            }
        }

        public void ConsumeItem(ItemCore item)
        {
            if (IsInInventory(item))
            {
                //TODO notice
                Siren.EditorNotice("consuming " + item.GetEditorName());
                items.Remove(item);
                InventoryManager.Instance.UpdateVisual();
            } else
            {
                Siren.EditorWarn("trying to consume item " + item.GetEditorName() + ", but its not in inventory");
            }
        }

    }
}
