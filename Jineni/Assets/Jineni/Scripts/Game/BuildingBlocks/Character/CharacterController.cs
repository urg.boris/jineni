﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;
using Jineni.Game;
using UnityEngine.Serialization;
using System;
using Jineni.DialogSystem.Graph;

#if UNITY_EDITOR
using UnityEditor.Animations;
using UnityEditor;
#endif

namespace Jineni.Game.BuildingBlocks
{
    public class CharacterController : MonoBehaviour, ICoreStateApliable
    {
        [FormerlySerializedAs("characterSoul")]
        public CharacterCore character;
        public Animator animator;
        public Collider2D colliderD2;

        float fadeTime = 0.5f;
        
        
        /*
        public string[] activities = new string[0];
        public int currentActivityIndex = 0;
        */
        
        [Header("!!Click Save Possibilities After Edit!!")]
        public ActivitiesStorage activitiesStorage;     
        [FormerlySerializedAs("possibleCharacterTransforms")]
        public TransformsStorage transformsStorage;
        
            
        //public Vector position;

        [HideInInspector]
        public List<string> stateNames = new List<string>(10);
        [HideInInspector]
        public int[] stateHashes;
#if UNITY_EDITOR
        [HideInInspector]
        public string corruptionReport;
#endif

        bool currentlyMouseInteractive = false;
        bool currentlyTalkative = false;

        SpriteRenderer[] allSprites;
        Color transparentColor = new Color(1, 1, 1, 0);
        Color opaqueColor = new Color(1, 1, 1, 1);


        void GetSpritesRefs()
        {
            if (allSprites == null)
            {
                allSprites = GetComponentsInChildren<SpriteRenderer>();
            }            
        }

        void Awake()
        {
            /*if (character == null)
            {
                //Debug.Log(name);
            }*/
            Assert.IsNotNull(character);
            GetSpritesRefs();
            //Assert.IsNotNull(boxCollider);
        }



        public void AnimateVisibilityChange()
        {
            if(character.visible==true)
            {
                StartCoroutine(AppearInLocation());
            } else
            {
                StartCoroutine(DisappearInLocation());
            }
        }

        void ApplyTransparency(float alpha)
        {
            GetSpritesRefs();
           /* if (allSprites == null || allSprites.Length==0)
            {

                //Debug.Log(name);
            }*/
            alpha = Mathf.Clamp01(alpha);
            Color currentColor = new Color(1, 1, 1, alpha);
            for (int i = 0; i < allSprites.Length; i++)
            {
                allSprites[i].color = currentColor;
            }
        }

        IEnumerator AppearInLocation()
        {
           // Debug.Log("please apper");
            ApplyTransparency(0);
            float elapsedTime = 0.0f;
            float ratio;
            //Color captionColor = caption.color;
            //Color backgroundColor = someTimePassedBackground.color;
            //float fadeTime = 0.5f;
            while (elapsedTime < fadeTime)
            {
                yield return new WaitForEndOfFrame();
                elapsedTime += Time.deltaTime;
                ratio = Mathf.Clamp01(elapsedTime / fadeTime);
                //Debug.Log("ratio" + ratio);
                ApplyTransparency(ratio);
                /*backgroundColor.a = ratio;
                someTimePassedBackground.color = backgroundColor;
                captionColor.a = ratio;
                caption.color = captionColor;*/
            }
            ApplyTransparency(1);
            //StartCoroutine(DisappearInLocation());
            yield break;
        }

        private IEnumerator DisappearInLocation()
        {
            //Debug.Log("please disapper");
            ApplyTransparency(1);
            float elapsedTime = 0.0f;
            //float fadeTime = 1f;
            float ratio;
            while (elapsedTime < fadeTime)
            {
                yield return new WaitForEndOfFrame();
                elapsedTime += Time.deltaTime;
                ratio = 1.0f - Mathf.Clamp01(elapsedTime / fadeTime);
                //Debug.Log("ratio" + ratio);
                ApplyTransparency(ratio);
            }
            ApplyTransparency(0);
            //StartCoroutine(AppearInLocation());
            yield break;
        }

        public void ApplyVisibility()
        {
            if (character.visible)
            {
                ApplyTransparency(1);
            }
            else
            {
                ApplyTransparency(0);
            }
            /* for (int i = 0; i < transform.childCount; i++)
             {
                 transform.GetChild(i).gameObject.SetActive(character.visible);
             }*/
        }

        public void ApplyCoreState()
        {
            ReadyCharacterOnLocationLoad();
        }

        public void ReadyCharacterOnLocationLoad()//ActivitiesForOneCharacterInLocation characterActivities, TransformsForOneCharacterInLocation characterTransforms//string[] possibleCharacterActivities
        {
            //UpdateAnimatorStatesList();
            ApplyMouseInteractivity();
            ApplyTalkativity();
            ApplyVisibility();
            //!! for location reloading
            ReloadPossibilitiesFromLocation();
            ApplyCharacterActivity();
            ApplyCharacterTransform();
        }

        public void ReloadPossibilitiesFromLocation()
        {
            transformsStorage = LocationController.Instance.location.GetTransformsFor(character);
            activitiesStorage = LocationController.Instance.location.GetActivitiesFor(character);
        }
        bool LocationDefinedCheck()
        {
            if (LocationController.Instance == null || LocationController.Instance.location == null)
            {
                Assert.IsTrue(false, "there is no locationcontroller or locationCore on scene");
                Siren.Error("there is no locationcontroller or locationCore on scene");
                return false;
            }
            return true;
        }
        
        public int HowManyActivitiesAreThere()
        {
            if(!LocationDefinedCheck())
            {
                return 0;
            }
            ActivitiesStorage storage = LocationController.Instance.location.GetActivitiesFor(character);
            if (storage == null)
            {
                return 0;
            }
            else
            {
                return storage.GetCount();
            }
        }

        public int HowManyTransformsAreThere()
        {
            if (!LocationDefinedCheck())
            {
                return 0;
            }
            TransformsStorage storage = LocationController.Instance.location.GetTransformsFor(character);
            if (storage == null)
            {
                return 0;
            } else
            {
                return storage.GetCount();
            }
        }

        public int GetCurrentActivityIndex()
        {
            ActivitiesStorage storage = LocationController.Instance.location.GetActivitiesFor(character);
            return storage.currentIndex;
        }

        public bool IsInActivityState(int index)
        {
            if(GetCurrentActivityIndex()==index)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ApplyCharacterActivity()
        {
            //Debug.Log(name);
            if (animator == null)
            {
                return;
            }
            //if not filled
          /*  if(stateNames.Count == 0)
            {
                UpdateAnimatorStatesList();
            }*/
            string activityName = GetCurrentActivityName();
            
            if (activityName == null)
            {
                if (activitiesStorage != null && activitiesStorage.currentIndex != -1)
                {
                    Siren.EditorNotice(name + " cant get current activity, defined corectly?");

                } else
                {
                    //its ok probably
                }
                return;

            }
            //Debug.Log(name + "  " + activityName+ " "+ stateNames.Count);

            if (stateNames.Contains(activityName))
            {
                animator.Play(activityName);
            } else
            {
                //!! stay in the same anim
                Siren.EditorWarn(name + "'s animator does not have animation " + activityName);
            }
        }

        public void ApplyCharacterTransform()
        {
            //Debug.Log("ApplyCharacterTransform"+character.GetEditorName());
            if (transformsStorage == null)
            {
                //Siren.EditorNotice(name + "'s transformsStorage not set ");
                return;
            }

            ConcreteTransform currentTransform = transformsStorage.GetCurrent();
            //!! can be null if indef is -1
            //Assert.IsNotNull(currentTransform);
            if (currentTransform!=null)
            {
                //Debug.Log("jako ze ok" + currentTransform.name);
                transform.localPosition = currentTransform.position;
                transform.rotation = currentTransform.rotation;
                transform.localScale = currentTransform.scale   ;
            }
        }

        public string GetCurrentActivityName()
        {
            LocationDefinedCheck();
            ActivitiesStorage storage = LocationController.Instance.location.GetActivitiesFor(character);
            if (storage == null)
            {
                return null;
            }
            return storage.GetCurrent();
        }



        public void ApplyMouseInteractivity()
        {
            currentlyMouseInteractive = character.mouseInteractive;
            if(colliderD2!=null) { 
                if(currentlyMouseInteractive)
                {
                    colliderD2.enabled = true;
                } else
                {
                    colliderD2.enabled = false;
                }
            }
        }

        public void ApplyTalkativity()
        {
            currentlyTalkative = character.talkative;
        }

        public bool ColliderDefined()
        {
            return colliderD2 != null;
        }

        

        private void OnMouseEnter()
        {
            if (ColliderDefined())
            {
                //Debug.Log(name+ GameManager.Instance.CanCharacterInteract()+"  "+ currentlyMouseInteractive);
                if (GameManager.Instance.CanCharacterInteract())
                {
                    if (currentlyMouseInteractive == true)
                    {
                        GameManager.Instance.ChangeCursorToInteractive();
                    }
                }
            }
        }

        //!!for right click-examine
        private void OnMouseOver()
        {
            if (ColliderDefined())
            {
                if (GameManager.Instance.CanCharacterInteract())
                {
                    if (currentlyMouseInteractive == true)
                    {
                        if (Input.GetMouseButtonDown(1))
                        {
                            DialogManager.Instance.StartExamination(character);
                        }
                    }
                }
            }
        }

        void OnMouseExit()
        {
            //!!nemelo by byt omezene interactivitou proste zmizi, kdyz bude vedle
            /* if (GameManager.Instance.CanCharacterInteract())
             {*/
            if (ColliderDefined())
            {
                GameManager.Instance.ChangeCursorToDefault();
            }
            //}
        }

        private void OnMouseDown()
        {
            if (ColliderDefined())
            {
                if (Input.GetMouseButtonDown(1))
                {
                    //!!figurka slona
                    Debug.Log("is second");
                }
                //TODO spolecne s game managerem vyresit kdy a jak muzou nastat ktery stavy akdy a jaky podminky, asi bude natvrdo, zatim nemenit, po dohode
                if (GameManager.Instance.CanCharacterInteract())
                {
                    if (currentlyTalkative == true)
                    {
                        StartDialog();
                    }
                }
            }
        }



        void StartDialog()
        {
            if (character.currentDialogEntryPoint != null)
            {
                DialogManager.Instance.StartDialog(character.currentDialogEntryPoint);
                //TODO talk if can or hadle talking by dialogcontroller, but charactercontroller sould set bool on initiallization if can talk(have talk animation)
                //animator.Play("talk");
            }
            else
            {
                //!!it just doesnt start
                //Assert.IsTrue(false, "character " + character.GetEditorName() + " nema entry point");
                //Siren.Error("character " + character.GetEditorName() + " nema entry point");
            }
        }

        /*void StartExamineDialog()
        {
            
            if (character.currentExamineEntryPoint != null)
            {
                DialogManager.Instance.StartDialog(character.currentExamineEntryPoint);
            }
            else
            {
                Assert.IsTrue(false, "character " + character.GetEditorName() + " nema examine entry point");
                Siren.Error("character " + character.GetEditorName() + " nema examine entry point");
            }
        }*/


        //======================================EDIOTR=======================
#if UNITY_EDITOR

        public void UpdateAnimatorStatesList()
        {
            //!! in case on i.e static no anim object
            if (animator == null)
            {
                stateHashes = new int[0];
                stateNames = new List<string>();
                return;
            }
            AnimatorController animatorController = (animator.runtimeAnimatorController as AnimatorController);

            if (animatorController == null)
            {
                stateHashes = new int[0];
                stateNames.Clear();
                return;
            }

            AnimatorControllerLayer[] allLayer = animatorController.layers;
            List<int> hashes = new List<int>();
            List<string> names = new List<string>();

            for (int i = 0; i < allLayer.Length; i++)
            {
                ChildAnimatorState[] states = allLayer[i].stateMachine.states;

                for (int j = 0; j < states.Length; j++)
                {
                    hashes.Add(Animator.StringToHash(states[j].state.name));
                    names.Add(states[j].state.name);

                }
            }

            stateHashes = new int[hashes.Count];
            stateNames.Clear();// = new List<string>(hashes.Count);

            for (int i = 0; i < hashes.Count; i++)
            {
                stateHashes[i] = hashes[i];
                stateNames.Add(names[i]);// names[i];
            }
        }

        public void SetDefaultTransform()
        {
            ReloadPossibilitiesFromLocation();
            if(transformsStorage.GetCount()==0)
            {
                Siren.EditorWarn("no transforms");
                return;
            }
            ConcreteTransform defaultTransform = transformsStorage.transforms[0];
            transform.position = defaultTransform.position;
            transform.rotation = defaultTransform.rotation;
            transform.localScale = defaultTransform.scale;
        }

        public void SetCurrentTransform()
        {
            ReloadPossibilitiesFromLocation();
            ConcreteTransform defaultTransform = transformsStorage.GetCurrent();
            transform.position = defaultTransform.position;
            transform.rotation = defaultTransform.rotation;
            transform.localScale = defaultTransform.scale;
        }

        //!!for editor save, probably wont be neccesary in game
        public void UpdateLists()
        {
            LocationDefinedCheck();
            ApplyMouseInteractivity();
            ApplyTalkativity();
            ReloadPossibilitiesFromLocation();
            UpdateAnimatorStatesList();
        }



        public bool IsCorrupted()
        {
            UpdateLists();
            string wholeName = name + "-" + character.editorName;
            if (character == null)
            {
                corruptionReport = wholeName + "characterCore must be defined";
                return true;
            }
            if (colliderD2 == null)
            {
                Siren.EditorNotice(wholeName + ": boxCollider not set ");
            }

            if (animator == null)
            {
                Siren.EditorNotice(wholeName + ": animator not set ");
            } else
            {
                if (stateNames.Count == 0 && HowManyActivitiesAreThere()==0)
                {
                    Siren.EditorNotice(wholeName + ": animator does not have any animations and no activities set, thats ok just to know");

                } else
                {
                    Siren.EditorWarn(wholeName+": animator have " + stateNames.Count + " animations and character have " + HowManyActivitiesAreThere() + " defined activities, are all activities and animations defined?");
                }
            }
            //!!todo uncoment
            /*
            if (character.currentDialogEntryPoint == null)
            {
                Siren.EditorWarn(wholeName + ": no dialog entry point, dialog wont start");
                //return true;
            } else
            {
                if (character.currentDialogEntryPoint.parentGraph.IsCorrupted())
                {
                    corruptionReport = character.currentDialogEntryPoint.parentGraph.corruptionReport;
                    return true;
                }
            }
          
            if (character.currentExamineEntryPoint == null)
            {
                Siren.EditorWarn(wholeName + ": no examine entry point, generic will be used");
            } else
            {
                if (character.currentExamineEntryPoint.parentGraph.IsCorrupted())
                {
                    corruptionReport = character.currentExamineEntryPoint.parentGraph.corruptionReport;
                    return true;
                }
            }*/

            if (activitiesStorage!=null)
            {
                if(activitiesStorage.IsCorrupted())
                {
                    corruptionReport = activitiesStorage.corruptionReport;
                    return true;
                }
            }
            else
            {
                Siren.EditorNotice(wholeName + ": activitiesStorage not set - no possible activities");
            }

            if (transformsStorage != null)
            {
                if (transformsStorage.IsCorrupted())
                {
                    corruptionReport = transformsStorage.corruptionReport;
                    return true;
                }
            }
            else
            {
                Siren.EditorNotice(wholeName + ": transformsStorage not set - no possible transforms");
            }
            return false;
        }
#endif

    }
}
