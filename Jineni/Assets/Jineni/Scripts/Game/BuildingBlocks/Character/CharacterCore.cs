﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;
using Jineni.Game.BuildingBlocks.Internals;
using UnityEngine.Serialization;
using Jineni.Localization;
using Jineni.Utils;

namespace Jineni.Game.BuildingBlocks
{
    [CreateAssetMenu(menuName = "Jineni/new character", order = 2)]
    public class CharacterCore : EntryPointLinkable, IEditorNamable
    {
        public string editorName = "name for editor";
        public Translation[] names = new Translation[1];
        public int currentNameIndex = 0;

        [FormerlySerializedAs("currentEntryPoint")]
        public EntryPoint currentDialogEntryPoint;
        public EntryPoint currentExamineEntryPoint;
        public EntryPoint defaultDialogEntryPoint;
        public EntryPoint defaultExamineEntryPoint;

        public bool visible = true;
        public bool mouseInteractive = true;
        public bool talkative = true;

        public bool defaultVisible = true;
        public bool defaultInteractive = true;
        public bool defaultTalkative = true;


        public string GetCurrentName(Lang lang)
        {
            if (!ArrayHelper.IsIndexInArrayRange(currentNameIndex, name.Length))
            {
                Assert.IsTrue(false, "not in index range");
                //fallback
                return names[0].Get(lang);
            }
            return names[currentNameIndex].Get(lang);
        }

        public string GetEditorName()
        {
            return (uniqueId>100 ? "O" : "P") + uniqueId + " - "+ editorName;
        }

        public void SetNameIndex(int index)
        {
            if(!ArrayHelper.IsIndexInArrayRange(index, names.Length))
            {
                Assert.IsTrue(false, "cant change name index; out of boundaries; i=" + index + "; count=" + names.Length);
                return;
            }
            currentNameIndex = index;
        }

        public void SetActiveState(bool currentActiveState, bool immediately = false)
        {
            SetVisibility(currentActiveState, immediately);
            SetMouseInteractivity(currentActiveState, immediately);
            SetTalkativity(currentActiveState, immediately);
            /*
            if(currentActiveState == true)
            {
                
            }
            else
            {

            }*/
        }

        public void SetVisibility(bool yesNoProperty, bool immediately = false)
        {
            if(visible==yesNoProperty)
            {
                Siren.EditorNotice("character "+ GetEditorName()+" is already in visibility state=" + yesNoProperty.ToString());
            }
            visible = yesNoProperty;
            if(immediately) {
                LocationController.Instance.AminateCharacterVisibilityChange(this);
            }
        }

        public void SetMouseInteractivity(bool yesNoProperty, bool immediately = false)
        {
            if (mouseInteractive == yesNoProperty)
            {
                Siren.EditorNotice("character " + GetEditorName() + " is already in mouseInteractive state=" + yesNoProperty.ToString());
            }
            mouseInteractive = yesNoProperty;
            if (immediately)
            {
                LocationController.Instance.UpdateCharacterMouseInteractivity(this);
            }
        }

        public void SetTalkativity(bool yesNoProperty, bool immediately = false)
        {
            if (talkative == yesNoProperty)
            {
                Siren.EditorNotice("character " + GetEditorName() + "is already in taklativity state=" + yesNoProperty.ToString());
            }
            talkative = yesNoProperty;
            if (immediately)
            {
                LocationController.Instance.UpdateCharacterTalkativity(this);
            }
        }

        public void SetDialogEntryPoint(EntryPoint entryPoint)
        {
            currentDialogEntryPoint = entryPoint;
        }

        public void SetExamineEntryPoint(EntryPoint entryPoint)
        {
            currentExamineEntryPoint = entryPoint;
        }

        public override void ResetToDefaultState()
        {
            currentNameIndex = 0;
            currentDialogEntryPoint = defaultDialogEntryPoint;
            currentExamineEntryPoint = defaultExamineEntryPoint;
            visible = defaultVisible;
            mouseInteractive = defaultInteractive;
            talkative = defaultTalkative;
        }
    }
}
