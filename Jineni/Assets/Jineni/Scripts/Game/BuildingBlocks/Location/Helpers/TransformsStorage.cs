﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Utils;
using UnityEngine.Assertions;

namespace Jineni.Game.BuildingBlocks
{
    /// <summary>
    /// all possible transforms for one character in one location
    /// </summary>
    [System.Serializable]
    public class TransformsStorage
    {
        //public CharacterCore character;
        [SerializeField]
        public List<ConcreteTransform> transforms;
        public int currentIndex = -1;

#if UNITY_EDITOR
        [HideInInspector]
        public string corruptionReport;
#endif

        public TransformsStorage() //public TransformsForOneCharacterInLocation(CharacterCore character)
        {
            //this.character = character;
            transforms = new List<ConcreteTransform>();
        }

        public void AddNewTransform(Transform transform)
        {
            ConcreteTransform characterTransform = new ConcreteTransform(transform);
            transforms.Add(characterTransform);
        }

        public string[] GetNames()
        {
            if(GetCount()==0)
            {
                return new string[0];
            } else
            {
                string[] names = new string[transforms.Count];
                for (int i = 0; i < transforms.Count; i++)
                {
                    names[i] = transforms[i].name;
                }
                return names;
            }

        }

        public int GetCount()
        {
            if(transforms==null)
            {
                return 0;
            }
            return transforms.Count;
        }

        public void SetIndex(int index)
        {
            if(index==-1 || GetCount()==0)
            {
                return;
            }
            if (!ArrayHelper.IsIndexInArrayRange(index, GetCount()))
            {
                Assert.IsTrue(false, "cant change transform index, transform index not in boundaries;i=" + index + " length=" + GetCount());
                //!! cant change, stay on where you are
                return;
            }
            currentIndex = index;
        }

        public ConcreteTransform GetCurrent() {
            if (GetCount() == 0)
            {
                //Siren.EditorNotice("transforms not defined");
                return null;
            }

            if (currentIndex == -1)
            {
                //!!it means show no transform
                //Siren.EditorNotice("currentIndex == -1");
                return null;
            }

            
            if (!ArrayHelper.IsIndexInArrayRange(currentIndex, GetCount()))
            {
                //Assert.IsTrue(false, "cant change transform index, index not in boundaries;i=" + currentIndex + " length=" + GetCount());
                Siren.EditorWarn("cant change transform index, index not in boundaries;i=" + currentIndex + " length=" + GetCount());
                return null;
            }
            return transforms[currentIndex];
        }

        public bool HasSome()
        {
            return GetCount() > 0;
        }

#if UNITY_EDITOR

        public bool IsCorrupted()
        {
            if (currentIndex == 0 && GetCount() == 0)
            {
                return false;
            }

            if (!ArrayHelper.IsIndexInArrayRange(currentIndex, GetCount()))
            {
                corruptionReport = "index not in array range: " + currentIndex + " is not in - [0," + GetCount() + "]";
                return true;
            }
            for (int i = 0; i < transforms.Count; i++)
            {
                if (transforms[i].name == "")
                {
                    corruptionReport = "activity i=" + i + " name is empty string";
                    return true;
                }
            }
            
            return false;
        }

#endif  
    }
}
