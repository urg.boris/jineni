﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;
using Jineni.Game;
using UnityEngine.SceneManagement;
using UnityEditor;
using Jineni.EditorWindows;

namespace Jineni.Game.BuildingBlocks
{
    public class LocationController : MonoBehaviour, ICoreStateApliable
    {
        public LocationCore location;
        public CharacterController[] characterControllers;

        static LocationController _instance = null;
        public static LocationController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<LocationController>();

                    if (_instance == null)
                    {
                        //!! tady dy se mel bud vytvorit, nebo chyba - chyba mel by tam bejt
                        Assert.IsTrue(false, "LocationController nejni");
                        Siren.Error("LocationController nejni");
                    }
                }
                return _instance;
            }
        }

        void Awake()
        {
            Assert.IsNotNull(characterControllers);
            Assert.IsNotNull(location);
        }

        private void Start()
        {
#if UNITY_EDITOR
            UpdateRefLinks();
#endif            
        }

        public void Run()
        {
            GameManager.Instance.CurrentLocationChanged(location);
            for (int i = 0; i < characterControllers.Length; i++)
            {
                characterControllers[i].ReadyCharacterOnLocationLoad();
            }
            //Debug.Log(location.onOpenEntryPoint);
            if (location.onOpenEntryPoint != null)
            {
                DialogManager.Instance.StartDialog(location.onOpenEntryPoint);
            }
        }

        CharacterController GetCharacterControllerBy(CharacterCore character)
        {
            if (characterControllers == null)
            {
                Siren.EditorWarn("charactedControllernot defined");
                return null;
            }
            for (int i = 0; i < characterControllers.Length; i++)
            {
                if (characterControllers[i] == null)
                {
                    Siren.EditorWarn("charactedController[" + i + "] is null in location linking");
                    return null;
                }
                if (characterControllers[i].character == character)
                {
                    return characterControllers[i];
                }
            }
            return null;
        }



        public void ApplyCharacterActivity(CharacterCore character)
        {
            //Debug.Log("ApplyCharacterActivity");
            CharacterController controller = GetCharacterControllerBy(character);
            if(controller==null)
            {
                Siren.EditorWarn("characted not in location, cant change activity");
                return;
            }
            controller.ApplyCharacterActivity();
        }

        public void ApplyCharacterTransform(CharacterCore character)
        {
            CharacterController controller = GetCharacterControllerBy(character);
            if (controller == null)
            {
                Siren.EditorWarn("characted not in location, cant change transcform");
                return;
            }
            controller.ApplyCharacterTransform();
        }



        public void AminateCharacterVisibilityChange(CharacterCore character)
        {
            CharacterController controller = GetCharacterControllerBy(character);
            if (controller == null)
            {
                Siren.EditorWarn(character.name+" characted not in location, cant UpdateCharacterVisibility");
                return;
            }

            //TODO slow hide/show mechanism
            controller.AnimateVisibilityChange();
        }
        
        public void UpdateCharacterMouseInteractivity(CharacterCore character)
        {
            CharacterController controller = GetCharacterControllerBy(character);
            if (controller == null)
            {
                Siren.EditorWarn(character.name + " characted not in location, cant UpdateCharacterMouseInteractivity");
                return;
            }
            controller.ApplyMouseInteractivity();
        }

        public void UpdateCharacterTalkativity(CharacterCore character)
        {
            CharacterController controller = GetCharacterControllerBy(character);
            if (controller == null)
            {
                Siren.EditorWarn(character.name + " characted not in location, cant UpdateCharacterTalkativity");
                return;
            }
            controller.ApplyTalkativity();
        }

        public void ApplyCoreState()
        {
            ApplyCharactersCoreState();
        }
        

        public void ApplyCharactersCoreState()
        {
            for (int i = 0; i < characterControllers.Length; i++)
            {
                if (characterControllers[i] == null)
                {
                    Siren.EditorWarn("charactedController[" + i + "] is null in location linking");
                }
                characterControllers[i].ReadyCharacterOnLocationLoad();
            }
        }
#if UNITY_EDITOR
        public void UpdateRefLinks()
        {
           characterControllers = FindObjectsOfType<CharacterController>();
        }

        public void CheckIntegrity()
        {
            UpdateRefLinks();

            string sceneName = SceneManager.GetActiveScene().name;
            Siren.EditorQuestion("checking scene " + sceneName);
            if (location == null)
            {
                EditorGUIUtility.PingObject(this);
                Assert.IsTrue(false, "location cant be null");

            }
            if (characterControllers.Length == 0)
            {
                Siren.EditorWarn("zero characters on location?");
                EditorGUIUtility.PingObject(this);
            }
            else
            {
                CharacterController[] charactersOnLocation = FindObjectsOfType<CharacterController>();
                Assert.IsTrue(charactersOnLocation.Length == characterControllers.Length, "linked characters != characters on location - characters in location=" + charactersOnLocation.Length + "; linked locations=" + characterControllers.Length);

                for (int i = 0; i < characterControllers.Length; i++)
                {
                    if (characterControllers[i].IsCorrupted())
                    {
                        EditorGUIUtility.PingObject(characterControllers[i]);
                        Assert.IsTrue(false, "charactercontroller " + characterControllers[i].name + " is corupted:" + characterControllers[i].corruptionReport);
                        return;
                    }
                }
            }
            if (location.onOpenEntryPoint != null)
            {
                IntegrityCheckerEditor.TestOneDialog(location.onOpenEntryPoint.parentGraph);
            }
            Siren.EditorOkLog("scene " + sceneName + " looks ok");
        }
#endif
    }
}
