﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;

namespace Jineni.Game.GameStateBlocks
{
    [System.Serializable]
    public class ItemState
    {
        public string editorName;
        public int id;
        public int nameIndex;

        public EntryPointState examineEntryPoint;
    }
}
