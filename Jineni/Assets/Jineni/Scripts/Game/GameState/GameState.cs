﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.Game.GameStateBlocks
{
    public class GameState : ScriptableObject, IEditorNamable
    {
        public LocationState[] locations;
        public CharacterState[] characters;
        public ItemState[] items;
        public MultiOptionState[] multiOptions;
        public InventoryState inventory;
        public int currentLocationId;

        public string GetEditorName()
        {
            return name;
        }
    }
}
