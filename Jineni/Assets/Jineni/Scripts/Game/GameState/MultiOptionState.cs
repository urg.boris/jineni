﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;

namespace Jineni.Game.GameStateBlocks
{
    [System.Serializable]
    public class MultiOptionState
    {
        public string id;
        public List<DialogOptionState> dialogOptionStates;

        public bool IsNotEmpty() {
            return id != "";
        }
    }
}
