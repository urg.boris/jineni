﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;



namespace Jineni
{
    public static class Siren
    {
        static string AddSystemInfo(string message)
        {
            return message + " (time of message:" + Time.realtimeSinceStartup + ")";
        }
/*
        public static void Notice(string message)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.Log(AddSystemInfo(message));
#else
               //TODO log report
#endif
        }

        public static void Warn(string message)
        {
            #if UNITY_EDITOR
                //StackTrace stackTrace = new StackTrace();
            //string stackTraceString = stackTrace.ToString();
            
            UnityEngine.Debug.LogWarning(string.Concat("<color=orange>", AddSystemInfo(message), "</color>"));
            //todo editor wanr neobarvi se na oranz, pokud tam je stackTraceString, fakt nevim co je za divno
            //UnityEngine.Debug.LogWarning(string.Concat("<color=orange>", AddSystemInfo(message), stackTraceString, "</color>"));
            //UnityEngine.Debug.LogWarning("<color=orange>" +  + "</color>");
#else
                 //TODO log report
#endif
        }*/

        public static void Error(string message)
        {
            #if UNITY_EDITOR
                StackTrace stackTrace = new StackTrace();
                UnityEngine.Debug.LogError("<color=red>" + AddSystemInfo(message) + stackTrace.ToString() + "</color>");
                throw new System.Exception("logging error - stop game");
            #else
            //TODO SORY WE FUCKED UP, RELOAD
                             //TODO log report
            #endif
        }

        public static void EditorOkLog(string message)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.Log("<color=green>" + AddSystemInfo(message) + "</color>");
#endif
        }

        public static void EditorNotice(string message)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.Log(AddSystemInfo(message));
#endif
        }

        public static void EditorQuestion(string message)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.Log("<color=blue>???" + AddSystemInfo(message) + "???</color>");
#endif
        }



        public static void EditorWarn(string message)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.LogWarning("<color=orange>" + AddSystemInfo(message) + "</color>");
#endif
        }

        public static void EditorError(string message)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.LogError("<color=red>"+ AddSystemInfo(message) + "</color>");
#endif
        }

    }
}
