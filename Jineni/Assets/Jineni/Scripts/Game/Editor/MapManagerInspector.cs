﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Jineni.DialogSystem.Graph;
using Jineni.DialogSystem;
using UnityEngine.SceneManagement;
using Jineni.Utils;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Jineni.Game.BuildingBlocks;

namespace Jineni.Game.GameStateBlocks
{
    [CustomEditor(typeof(MapManager))]
    public class MapManagerInspector : Editor
    {
        MapManager mapManager;

        void OnEnable()
        {
            mapManager = serializedObject.targetObject as MapManager;
        }

        public override void OnInspectorGUI()
        {

            if (GUILayout.Button("Update Ref Lists"))
            {
                mapManager.UpdateRefList();
            }
            DrawDefaultInspector();



        }
    }
}