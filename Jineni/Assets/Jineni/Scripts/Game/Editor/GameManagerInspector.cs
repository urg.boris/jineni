﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jineni.Utils;
using Jineni.Game.BuildingBlocks;
using Jineni.Game.GameStateBlocks;

namespace Jineni.Game
{
    [CustomEditor(typeof(GameManager))]
    public class GameManagerInspector : Editor
    {
        GameManager gameManager;
        string stateName;

        void OnEnable()
        {
            gameManager = serializedObject.targetObject as GameManager;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Reset All To Default State"))
            {
                gameManager.ResetAllToDefaultState();
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label("Save Current Adjustements As Game State:");
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            stateName = GUILayout.TextField(stateName);
            if (GUILayout.Button("Save"))
            {
                gameManager.SaveGameStateToSO(stateName);
                stateName = "";
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            GUILayout.Label("Load Game State To Current Adjustements:");
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            gameManager.providedGameState = EditorGUILayout.ObjectField(gameManager.providedGameState, typeof(GameState), false) as GameState;
            if (GUILayout.Button("Load"))
            {
                gameManager.LoadProvidedGameState();
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();

            if (GUILayout.Button("Update Reference Links"))
            {
                gameManager.UpdateRefLinks();
                serializedObject.Update();
            }
        }
    }
}
