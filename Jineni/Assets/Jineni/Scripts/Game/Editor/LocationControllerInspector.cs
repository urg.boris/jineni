﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Jineni.DialogSystem.Graph;
using Jineni.DialogSystem;
using UnityEngine.SceneManagement;
using Jineni.Utils;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Jineni.EditorWindows;

namespace Jineni.Game.BuildingBlocks
{
    [CustomEditor(typeof(LocationController))]
    public class LocationControllerInspector : Editor
    {
        /*
        SerializedProperty characters;
        SerializedProperty location;
        SerializedProperty characterPositions;
        */
        LocationController locationController;
        LocationCore locationCore;


        void OnEnable()
        {
            locationController = serializedObject.targetObject as LocationController;
            locationCore = locationController.location;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Update Reference Links"))
            {
                UpdateRefLinks();
            }
            if (GUILayout.Button("Check Integrity"))
            {
                CheckIntegrity();
            }


            
            DrawDefaultInspector();

            if(locationCore != null)
            {
               // EditorGUILayout.BeginVertical();
                EditorGUIUtility.labelWidth = 200;
                EditorGUI.BeginChangeCheck();
                locationCore.onOpenEntryPoint = EditorGUILayout.ObjectField("On Open EntryPoint", locationCore.onOpenEntryPoint, typeof(EntryPoint), false) as EntryPoint;
                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(locationCore);
                }
               // EditorGUILayout.EndVertical();
            }

        }

        void UpdateRefLinks()
        {
            locationController.UpdateRefLinks();
            serializedObject.ApplyModifiedProperties();
        }
        void CheckIntegrity()
        {
            serializedObject.Update();
            locationController = serializedObject.targetObject as LocationController;
            locationController.CheckIntegrity();
        }
       
    }
}