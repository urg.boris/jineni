﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR

namespace Jineni.DialogSystem
{
    public static class DialogEditorStyles
    {
        // Textures
        public static Texture2D dot { get { return _dot != null ? _dot : _dot = Resources.Load<Texture2D>("xnode_dot"); } }
        private static Texture2D _dot;
        public static Texture2D dotOuter { get { return _dotOuter != null ? _dotOuter : _dotOuter = Resources.Load<Texture2D>("xnode_dot_outer"); } }
        private static Texture2D _dotOuter;
        public static Texture2D nodeBody { get { return _nodeBody != null ? _nodeBody : _nodeBody = Resources.Load<Texture2D>("node6"); } }
        private static Texture2D _nodeBody;
        public static Texture2D nodeHighlight { get { return _nodeHighlight != null ? _nodeHighlight : _nodeHighlight = Resources.Load<Texture2D>("xnode_node_highlight"); } }
        private static Texture2D _nodeHighlight;

        // Styles
        public static Styles styles { get { return _styles != null ? _styles : _styles = new Styles(); } }
        public static Styles _styles = null;
        public static GUIStyle OutputPort { get { return new GUIStyle(EditorStyles.label) { alignment = TextAnchor.UpperRight }; } }
        public class Styles
        {
            public GUIStyle inputPort, nodeHeader, nodeBody, tooltip, nodeHighlight, nodeHeaderStyle;

            public Styles()
            {
                GUIStyle baseStyle = new GUIStyle("Label");
                baseStyle.fixedHeight = 18;

                inputPort = new GUIStyle(baseStyle);
                inputPort.alignment = TextAnchor.UpperLeft;
                inputPort.padding.left = 10;

                nodeHeader = new GUIStyle();
                nodeHeader.alignment = TextAnchor.MiddleCenter;
                nodeHeader.fontStyle = FontStyle.Bold;
                nodeHeader.normal.textColor = Color.white;

                nodeBody = new GUIStyle();
                nodeBody.normal.background = DialogEditorStyles.nodeBody;
                nodeBody.border = new RectOffset(32, 32, 32, 32);
                nodeBody.padding = new RectOffset(16, 16, 4, 16);

                nodeHighlight = new GUIStyle();
                nodeHighlight.normal.background = DialogEditorStyles.nodeHighlight;
                nodeHighlight.border = new RectOffset(32, 32, 32, 32);

                nodeHeaderStyle = new GUIStyle();
                nodeHeaderStyle.alignment = TextAnchor.MiddleLeft;
                nodeHeaderStyle.fontStyle = FontStyle.Bold;
                nodeHeaderStyle.normal.textColor = Color.white;
                //nodeHeaderStyle.fixedHeight = 18;
                /*Texture2D tex = new Texture2D(2, 2);

                var fillColorArray = tex.GetPixels32();
                Color color = new Color(0, 1, 0);
                for (var i = 0; i < fillColorArray.Length; ++i)
                {
                    fillColorArray[i] = colors.fieldBackground;
                }

                tex.SetPixels32(fillColorArray);
                
                tex.Apply();
                nodeHeaderStyle.normal.background = tex;*/
            }
        }

        public class Colors
        {
            public Color undersea, blue, barared, lightGreen, lightPurple, lightRed, purle, red, green, orange, yellow, greenBlue, darkBlue, fieldBackground, complementaryGreenBlue;

            public Colors()
            {
                undersea = new Color(0.106f, 0.078f, 0.392f);
                blue = new Color(0.024f, 0.322f, 0.867f);
                barared = new Color(0.929f, 0.298f, 0.404f);
                lightGreen = new Color(0.639f, 0.796f, 0.220f);
                lightPurple = new Color(0.600f, 0.502f, 0.980f);

                lightRed = new Color(0.710f, 0.204f, 0.443f);
                purle = new Color(0.435f, 0.118f, 0.318f);
                red = new Color(0.918f, 0.125f, 0.153f);
                green = new Color(0.000f, 0.580f, 0.196f);

                orange = new Color(0.93f, 0.35f, 0.14f);
                yellow = new Color(0.97f, 0.62f, 0.12f);

                greenBlue = new Color( 0.07f,0.80f,  0.77f);
                complementaryGreenBlue = new Color(1f, 0.6f, 0.4f);
                darkBlue = new Color(0.34f,  0.35f, 0.73f);

                fieldBackground = new Color(0.77f, 0.90f, 0.22f, 0.5f);


            }
        }



        public static Colors colors { get { return _colors != null ? _colors : _colors = new Colors(); } }
        public static Colors _colors = null;

        //colors
        public static Color headerTextColor = Color.white;
        public static Color connectionColor = colors.greenBlue;// new Color(0.024f, 0.322f, 0.867f);

        public static Color entryPointColor = colors.red;
        public static Color replicColor = colors.green;
        public static Color multiColor = colors.blue;
        public static Color conditionalColor = colors.yellow;
        public static Color triggerColor = colors.lightRed;

        public static Color fieldBackground = colors.fieldBackground;
        public static Color randomConnection = colors.complementaryGreenBlue;











        //sizes
        public static float connectionLineSize = 4f;
    }
}
#endif