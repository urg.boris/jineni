﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.FlowControl.Conditions;
using UnityEngine.Serialization;
using UnityEngine.Assertions;
using Jineni.Utils;

namespace Jineni.DialogSystem.Graph
{
    public class Condition : Node
    {
        const int OUTCOMES_COUNT = 2;


        public ConcreteCondition condition;





        //===================================EDITOR===============================
#if UNITY_EDITOR

        //inherit
        public static Color backgroundColor = DialogEditorStyles.conditionalColor;
        private static int nodeWidth = 300;
        private static int nodeHeight = 150;

        public override void Init(Dialog parentGraph, Vector2 position, string name)
        {
            base.Init(parentGraph, position, name);
            GenerateNodeNameRect(GetWidth());
        }

        protected override Color GetBackgroundColor()
        {
            return backgroundColor;
        }

        protected override int GetHeight()
        {
            return nodeHeight;
        }

        protected override int GetWidth()
        {
            return nodeWidth;
        }

        public override void CreatePorts()
        {
            //!! kdyz se to da do vlastni fce tak to nefunguje, zjistit proc by bylo na dlouho
            inPort = ScriptableObject.CreateInstance<Port>();
            inPort.Init(this, Port.IO.In);
            inPort.name =string.Concat(name,"-", INPORT);
            AssetDatabase.AddObjectToAsset(inPort, this);
            

            outPorts = new List<Port>();
            for (int i = 0; i < OUTCOMES_COUNT; i++)
            {
                CreateOptionOutPort(i);
            }

            AssetDatabase.SaveAssets();
        }

        public override void RunAfterAssetSaved()
        {
            CreatePorts();

            condition = ScriptableObject.CreateInstance<ConcreteCondition>();
            condition.name = name + "-C";

            AssetDatabase.AddObjectToAsset(condition, this);
            //!!muze ukladat jen jednou dohromady s create portama
            AssetDatabase.SaveAssets();
        }

        private void CreateOptionOutPort(int i)
        {
            Port outPort = ScriptableObject.CreateInstance<Port>();
            outPort.Init(this, Port.IO.Out);
            outPort.name = string.Concat(name, "-", OUTPORT, i);
            AssetDatabase.AddObjectToAsset(outPort, this);
            //!! ukladani vramci RunAfterAssetSaved
            //AssetDatabase.SaveAssets();
            outPorts.Add(outPort);
        }

        public override void OnRemoveNode()
        {
            Object.DestroyImmediate(condition, true);
        }


        protected override void OnBodyGUI()
        {

            //GUILayout.BeginHorizontal();
            //contitionalType = (CONDITIONAL_TYPE)EditorGUILayout.EnumPopup("Co zjistuju?", contitionalType);
            //contitionalType
            //conditionTester = EditorGUILayout.ObjectField(conditionTester, typeof(Character), false) as Character;
            // GUILayout.EndHorizontal();

            //!!saving object change
            EditorGUI.BeginChangeCheck();

            //condition = EditorGUILayout.ObjectField(condition, typeof(ConcreteCondition), false) as ConcreteCondition;
            condition.type = EditorGUILayout.ObjectField(condition.type, typeof(ConditionType), false) as ConditionType;
            GUILayout.Space(5);

            if(condition.type != null)
            {
                if (condition.type is CharacterIsInActivityState)
                {
                    EditorGUI.BeginChangeCheck();
                    condition.variable = EditorGUILayout.ObjectField(condition.variable, typeof(LocationCore), false) as LocationCore;
                    condition.character = EditorGUILayout.ObjectField(condition.character, typeof(CharacterCore), false) as CharacterCore;
                    if (EditorGUI.EndChangeCheck())
                    {
                        if (condition.variable != null && condition.character != null)
                        {

                            condition.stringOptions = (condition.variable as LocationCore).GetPossibleCharacterActivityNames(condition.character as CharacterCore);
                            //optionNames = (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore);
                            //triggerOption.Add(i, (triggers[i].variable as LocationCore).GetPossibleCharacterActivityNames(triggers[i].character as CharacterCore));
                        }

                    }

                    condition.intParam = EditorGUILayout.Popup(condition.intParam, condition.stringOptions != null ? condition.stringOptions : new string[0], GUILayout.Width(200));
                }
                else if (condition.type is LocationIsOpened)
                {
                    condition.variable = EditorGUILayout.ObjectField(condition.variable, typeof(LocationCore), false) as LocationCore;
                }
                else if (condition.type is CharacterIsActive)
                {
                    condition.variable = EditorGUILayout.ObjectField(condition.variable, typeof(CharacterCore), false) as CharacterCore;
                }
                else if(condition.type is IsInInventory)
                {
                    condition.variable = EditorGUILayout.ObjectField(condition.variable, typeof(ItemCore), false) as ItemCore;
                }
                else if (condition.type is OddnessReached)
                {
                    condition.intParam = EditorGUILayout.IntSlider(condition.intParam, 0, 100);
                }
                else
                {
                    Assert.IsTrue(false, condition.type.ToString() + "is not recognized condition type");
                }
     /*
                //!!jinak si editor nepamatuje po opetovnym loadu
                 if (lastConditionType != conditionType)
                 {
                     Debug.Log("refre");
                     lastConditionType = conditionType;
                    //!! tohle kombo je nutny !!!
                     EditorUtility.SetDirty(this);
                     AssetDatabase.SaveAssets();
                    //!!

                 }*/
            }

            //!!saving object change
            if (EditorGUI.EndChangeCheck())
            {
                SaveNodeParams();
                /*
                //!! tohle kombo je nutny !!!
                EditorUtility.SetDirty(this);
                AssetDatabase.SaveAssets();*/
            }

            DrawOptions();

        }

        void OnValueChanged()
        {
            Debug.Log("dasdas");
        }
        /*
        private void RemoveConnectionsOnOutPort(int i)
        {
            for (int j = 0; j < outPorts[i].connections.Count; j++)
            {
                parentGraph.RemoveConnection(outPorts[i].connections[j]);
            }
            outPorts.RemoveAt(i);

        }*/

        private void DrawOptions()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(5);
            EditorGUILayout.BeginVertical();
            for (var i = 0; i < OUTCOMES_COUNT; i++)
            {
                    
                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();

                GUIStyle rightStyle = GUI.skin.GetStyle("Label");
                rightStyle.alignment = TextAnchor.MiddleRight;
                GUILayout.Label(i==0?"ANO":"NE", rightStyle);


                //!!use corect color
                Color lastColor = GUI.color;
                GUI.color = (i == 0 ? DialogEditorStyles.colors.green : DialogEditorStyles.colors.red);

                outPorts[i].DrawWithOffset(GUILayoutUtility.GetLastRect().center.y);

                //!!use corect color
                GUI.color = lastColor;

                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
                GUILayout.Space(4);
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        protected override void DrawPorts()
        {
            Color lastColor = GUI.color;
            GUI.color = GetBackgroundColor();

            inPort.Draw();
            //!!outports drawn in DrawOptions()

            GUI.color = lastColor;
        }

        public override bool IsCorrupted()
        {
            if (base.IsCorrupted())
            {
                return true;
            }

            if (!inPort.IsConnected())
            {
                corruptionReport = "inport is not connected, redundant?";
                return true;
            }

            if (!outPorts[0].IsConnected())
            {
                corruptionReport = "outport 1 is not connected";
                return true;
            }

            if (!outPorts[1].IsConnected())
            {
                corruptionReport = "outport 2 is not connected";
                return true;
            }

            if (condition.type == null)
            {
                corruptionReport = "condition not set";
            }

            bool containsVariable = !(condition.type is OddnessReached);

            if (containsVariable)
            {
                if (condition.variable == null)
                {
                    corruptionReport = "condition variable not set";
                    return true;
                }
            }

            return false;
        }
#endif

        //============================================== FOR GAME ==================================
        public Node GetNextNodeFromOption(int optionIndex)
        {
            if (!ArrayHelper.IsIndexInArrayRange(optionIndex, outPorts.Count))
            {
                //!! hra je dialog driven, kdyz je tohle posrany tak bude asi i cela hra
                Assert.IsTrue(false, "using GetNextNodeFromOption(), but optionIndex > outPorts.Count, bigger index than ports(options)");
                Siren.Error("using GetNextNodeFromOption(), but optionIndex > outPorts.Count, bigger index than ports(options)");
            }


            return GetNodeByPortIndex(optionIndex);
        }

        public bool IsTrue()
        {
            return condition.IsTrue();
        }

 
    }
}
