﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Jineni.DialogSystem.Graph
{


    public class EntryPoint : Node {



        //===================================EDITOR===============================
#if UNITY_EDITOR
        private static int nodeWidth = 180;
        private static int nodeHeight = 100;

        public static Color backgroundColor = DialogEditorStyles.entryPointColor;

        public string note;
        private Vector2 scroll;
        public override void Init(Dialog parentGraph, Vector2 position, string name)
        {
            base.Init(parentGraph, position, name);
            GenerateNodeNameRect(GetWidth());
        }
        

        protected override int GetWidth()
        {
            return nodeWidth;
        }

        protected override int GetHeight()
        {
            return nodeHeight;
        }

        protected override Color GetBackgroundColor()
        {
            return backgroundColor;
        }

        public override void CreatePorts()
        {
            inPort = null;
            outPorts = new List<Port>(1);

            //!! kdyz se to da do vlastni fce tak to nefunguje, zjistit proc by bylo na dlouho
            Port outPort = ScriptableObject.CreateInstance<Port>();
            outPort.hideFlags = HideFlags.None;
            outPort.Init(this, Port.IO.Out);
            outPort.name = string.Concat(name, "-", OUTPORT);
            outPorts.Add(outPort);

            AssetDatabase.AddObjectToAsset(outPort, this);
            AssetDatabase.SaveAssets();

            
        }

        protected override void OnBodyGUI()
        {
            EditorStyles.textField.wordWrap = true;
            scroll = EditorGUILayout.BeginScrollView(scroll);
            note = EditorGUILayout.TextArea(note, GUILayout.ExpandHeight(true));
            EditorGUILayout.EndScrollView();
        }

        protected override void DrawPorts()
        {
            Color lastColor = GUI.color;
            GUI.color = GetBackgroundColor();

            outPorts[0].Draw();

            GUI.color = lastColor;
        }

        protected override void OnClickRemoveNode()
        {
           // parentGraph.RemoveEntryPoint(this);
            base.OnClickRemoveNode();
        }

        public override bool IsCorrupted()
        {
            if (base.IsCorrupted())
            {
                return true;
            }
            if (!outPorts[0].IsConnected())
            {
                corruptionReport = "outport is not connected, redundant?";
                return true;
            }
            return false;
        }
#endif
    }
}
