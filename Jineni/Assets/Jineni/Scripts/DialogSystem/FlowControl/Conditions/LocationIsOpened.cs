﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Conditions
{
    [CreateAssetMenu(fileName = "LocationIsOpened", menuName = "Jineni/Internal/Condition/LocationIsOpened", order = 3)]
    public class LocationIsOpened : ConditionType
    {
        public override bool IsTrue(ScriptableObject variable)
        {
            return (variable as LocationCore).isOpened;
        }
    }
}
