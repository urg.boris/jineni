﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{

    //[CreateAssetMenu(fileName = "OptionChangeTrigger", menuName = "Jineni/Internal/Trigger/OptionChangeTrigger", order = 41)]
    public abstract class OptionChangeTrigger : TriggerType
    {
        public override void Invoke(ScriptableObject variable)
        {
            Assert.IsTrue(false, this.GetType().ToString() + " must have three arguments");
            Siren.Error(this.GetType().ToString() + " must have three arguments");
        }

        public abstract void Invoke(ScriptableObject variable, int intParam);

        public string[] GetOptionTextsFrom(MultiOption multiOption)
        {
            return multiOption.GetDialogOptionTexts();
        }
    }
}
