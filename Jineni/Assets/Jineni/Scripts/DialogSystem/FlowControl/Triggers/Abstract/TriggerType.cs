﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    public abstract class TriggerType : ScriptableObject
    {
        public abstract void Invoke(ScriptableObject variable);

    }
}
