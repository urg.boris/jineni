﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeTalkativityLater", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeTalkativityLater")]
    public class CharacterChangeTalkativityLater : CharacterChangeYesNoPropertyTrigger
    {

        public override void Invoke(ScriptableObject variable, bool yesNoProperty)
        {
            (variable as CharacterCore).SetTalkativity(yesNoProperty);
        }
    }
}
