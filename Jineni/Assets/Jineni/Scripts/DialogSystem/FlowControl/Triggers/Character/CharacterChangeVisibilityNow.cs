﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeVisibilityNow", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeVisibilityNow")]
    public class CharacterChangeVisibilityNow : CharacterChangeYesNoPropertyTrigger
    {

        public override void Invoke(ScriptableObject variable, bool yesNoProperty)
        {
            (variable as CharacterCore).SetVisibility(yesNoProperty, true);
        }
    }
}
