﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeTalkativityNow", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeTalkativityNow")]
    public class CharacterChangeTalkativityNow : CharacterChangeYesNoPropertyTrigger
    {

        public override void Invoke(ScriptableObject variable, bool yesNoProperty)
        {
            (variable as CharacterCore).SetTalkativity(yesNoProperty, true);
        }
    }
}
