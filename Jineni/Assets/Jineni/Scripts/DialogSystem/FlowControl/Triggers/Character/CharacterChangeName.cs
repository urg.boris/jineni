﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers  
{
    [CreateAssetMenu(fileName = "CharacterChangeName", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeName")]
    public class CharacterChangeName : CharacterTrigger
    {
        public void Invoke(ScriptableObject variable, int index)
        {
            (variable as CharacterCore).SetNameIndex(index);
        }
    }
}
