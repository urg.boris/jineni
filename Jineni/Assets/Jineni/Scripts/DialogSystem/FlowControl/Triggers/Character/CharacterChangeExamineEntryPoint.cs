﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.DialogSystem.Graph;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "CharacterChangeExamineEntryPoint", menuName = "Jineni/Internal/Trigger/Character/CharacterChangeExamineEntryPoint")]
    public class CharacterChangeExamineEntryPoint : CharacterTrigger
    {
        //public ScriptableObject tester;

        public void Invoke(ScriptableObject variable, EntryPoint entryPoint)
        {
            (variable as CharacterCore).SetExamineEntryPoint(entryPoint);
        }
    }
}
