﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.Game;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "LocationChangeCharacterTransformNow", menuName = "Jineni/Internal/Trigger/LocationCharacter/LocationChangeCharacterTransformNow")]
    public class LocationChangeCharacterTransformNow : LocationChangeCharacterTransformTrigger
    {
        public override void Invoke(ScriptableObject variable, CharacterCore character, int index)
        {
            GameManager.Instance.ChangeLocationCharacterTransform(variable as LocationCore, character , index, true);
        }
    }
}
