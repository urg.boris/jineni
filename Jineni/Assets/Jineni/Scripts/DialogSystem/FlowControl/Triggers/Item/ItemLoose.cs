﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemLoose", menuName = "Jineni/Internal/Trigger/Item/ItemLoose")]
    public class ItemLoose : InventoryTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            inventory.LooseItem(variable as ItemCore);
        }
    }
}
