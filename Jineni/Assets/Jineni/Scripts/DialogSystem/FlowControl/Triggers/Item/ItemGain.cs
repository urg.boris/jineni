﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemGain", menuName = "Jineni/Internal/Trigger/Item/ItemGain")]
    public class ItemGain : InventoryTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            inventory.GainItem(variable as ItemCore);
        }
    }
}
