﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemUse", menuName = "Jineni/Internal/Trigger/Item/ItemUse")]
    public class ItemUse : InventoryTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            inventory.UseItem(variable as ItemCore);
        }
    }
}
