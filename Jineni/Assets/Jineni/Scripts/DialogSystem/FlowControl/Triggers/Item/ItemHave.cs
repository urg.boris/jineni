﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "ItemHave", menuName = "Jineni/Internal/Trigger/Item/ItemHave")]
    public class ItemHave : InventoryTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            inventory.HaveItem(variable as ItemCore);
        }
    }
}
