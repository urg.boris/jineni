﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using Jineni.Game;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "LocationVisit", menuName = "Jineni/Internal/Trigger/Location/LocationVisit")]
    public class LocationVisit : LocationTrigger
    {
        public override void Invoke(ScriptableObject variable)
        {
            GameManager.Instance.VisitLocation(variable as LocationCore);
        }
    }
}
