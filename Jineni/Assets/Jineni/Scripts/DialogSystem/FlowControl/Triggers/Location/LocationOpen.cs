﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "LocationOpen", menuName = "Jineni/Internal/Trigger/Location/LocationOpen")]
    public class LocationOpen : LocationTrigger
    {
        //public ScriptableObject tester;

        public override void Invoke(ScriptableObject variable)
        {
            (variable as LocationCore).OpenNotice();
        }
    }
}
