﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.Game.BuildingBlocks;
using UnityEngine.Assertions;

namespace Jineni.DialogSystem.FlowControl.Triggers
{
    [CreateAssetMenu(fileName = "DelayTrigger", menuName = "Jineni/Internal/Trigger/General/DelayTrigger")]
    public class DelayTrigger : FloatTrigger
    {
        public override void Invoke(float time)
        {
            //!!  is handled by dialog controller
            Assert.IsTrue(false, GetType().Name + " should be handeled by dialog manager");
        }
    }
}
