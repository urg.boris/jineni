﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jineni.DialogSystem.Graph;
using Jineni.Game;

namespace Jineni.DialogSystem.FlowControl.Triggers
{

    [CreateAssetMenu(fileName = "OptionMakeActive", menuName = "Jineni/Internal/Trigger/General/OptionMakeActive")]
    public class OptionMakeActive : OptionChangeTrigger
    {
        public override void Invoke(ScriptableObject variable, int intParam)
        {
            MultiOption multiOption = (variable as MultiOption);
            multiOption.SetOptionState(intParam, true);
            GameManager.Instance.MultiOptionChagned(multiOption);
        }
    }
}
