﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Jineni.Game;

namespace Jineni.Localization
{
    [System.Serializable]
    public class Translation
    {
        public string cs = "";
        public string en = "";

        public string Get(Lang locale)
        {
            if (locale == Lang.cs)
            {
                return cs;
            } else
            {
                return en;
            }
        }

        public void Set(Lang locale, string value)
        {
            if (locale == Lang.cs)
            {
                cs = value;
            }
            else
            {
                en = value;
            }
        }
    }


}
