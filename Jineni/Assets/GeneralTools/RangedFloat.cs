﻿using System;

namespace General
{
    [Serializable]
    public struct RangedFloat
    {
        public float minValue;
        public float maxValue;
    }
}