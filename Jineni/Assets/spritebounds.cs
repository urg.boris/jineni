﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class spritebounds : MonoBehaviour
{
    [Range(0,1f)]
    public float paralaxPosition;
    public float leftOffsetLimit;
    public float rightOffsetLimit;

    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        float spriteWidth = spriteRenderer.bounds.size.x;
        //size in Units
        //Vector3 itemSize = spriteRenderer.bounds.size;
        //Debug.Log(Camera.main.pixelWidth+"  "+ Camera.main.pixelHeight); //, itemSize
        /*
        float pixelsPerUnit = spriteRenderer.sprite.pixelsPerUnit;


        itemSize.y *= pixelsPerUnit;
        itemSize.x *= pixelsPerUnit;*/
        //Debug.Log(itemSize);
        float screenHeightInUnits = Camera.main.orthographicSize * 2;
        float screenWidthInUnits = screenHeightInUnits * Screen.width / Screen.height;
        /*
        float cameraScreenSizeInPx = Camera.main.orthographicSize * 2.0f * pixelsPerUnit;
        float cameraScreenSizeInUnits = Camera.main.orthographicSize * 2.0f;*/
        Debug.Log(spriteWidth + " "+ screenWidthInUnits);

        Assert.IsTrue(spriteWidth >= screenWidthInUnits);

        float xossfet = (spriteWidth - screenWidthInUnits)/2;
        leftOffsetLimit = xossfet;
        rightOffsetLimit = -xossfet;


    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = transform.position;
        newPos.x = Mathf.Lerp(leftOffsetLimit, rightOffsetLimit, paralaxPosition);
        transform.position = newPos;
    }
}
