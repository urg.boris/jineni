﻿


SLOVNÍK
	prozkoumat = kliknutí druhým čudlem myši
	dialog = rozhovor, může také tvořit čast rozhovoru, neboť Celý strom Rozhovorů je vlastně sloučení všech malých rozhovorů
	replica = 	jedna hláška/replika rozhovoru,
				která v sobě obsahuje ukazatel pointTo,
					ukazatel pointTo má buď odkaz na následující repliku
					nebo má ukazate na víc replik, v tom případě je hráčí nabídnuto, kterou cestou se rozhovor bude ubírat
	trigger = součástí repliky může být i vyvolání určité akce - trigger
	přechodová šipka = šipka na hracím plátně, krerá hráče přesune na jiné místo
	mapa = obsahuje "objevená" místa, na která se dá pomocí mapy přesouvat
	I - item = předmět inventáře
	P - character=person=postava = postava hry
	O - dialog object = nezivy objekt se kterym si muzes povidat, to samy jako character pro prehlednost jinak pojmenovane
	L - location = lokace, místo ve hře, přistupné přes mapu, nebo přechodové šipky pokud je hráč vedle
	



REPLIKY (pismena v idecku jsou velkymi a oddeleny "_")

	
	-L = location
	-P = person = character
	-O = dialog object 
	-E = examine, prozkoumat, druhý tlačítko myši
	-I - item
	-R - random, nahodny vyber nektere z moznosti
	
CONDITIONAL

  LOKACE
    if 
      isOnMap  
      isOpened
      isActive(isOpened||isOnMap)
      
  CHARACTER
    if
      isVisible
      isMouseInteractive
      isTalkative
      isActive(isTalkative)
      
  ITEM
    if
      isInInventory
      
  KRUMLOFIKACE
    if
      optionIsOpened

		

TRIGGERY - TODO


	pointsTo = aktivuje/deaktivuje možnost v rozvetveni dialogu
		forReplic = id repliky uvnitr ktere jsou moznosti (<replic><pointsTo> v ktere jsou replicNode)
		replicNodeId = id replicNode = ukazatel na na dalsi repliky
			nelze de/aktivovat replicNode pokud ma pointsTo pouze jednu replicNode - defenzivni ochrana divnych chyb
			
			<forPointsTo forReplic="l12_ch42_ep1" replicNodeId="l12-c42-t4">
				<activate/>
			</forPointsTo>	



	item
		gain = hráč je informován o tom, že získal předmět
		loose = hráč je informován o tom, že ztratil předmět
		use = hráč je informován o tom, že použil předmět
		isInInventory(0/1) = hráč není informován o tom, že ma/nema předmět






	character/object = character a object sou pro počítač ekvivalentní, rozděluju je jen pro naši snadnější orientaci
		actualEntryPoint = replika, kterou bude postava začínat rozhovor pokud se na ní klikne
		waitingAnimation = aktuální animace čekání/neaktivnosti postavy/objektu("animace" může být i jedno snímková)

		TODO defaultni hodnoty jako waitingAnimation=1
			hide=0
			hideMouseInteractivity=0
			makeInactive=0

		akce - SLOŽITÉ!
			postava/objekt můžé být:
				a) 	shown(0/1)
					viditelný / neviditelný pro hráče
					
				b)	mouseInteractivity(0/1)
					viditelný / neviditelný pro myš(když se na něj najede myší, změní se kurzor a je možné ho prozkoumat)
					
				c)	talkative(0/1)
					schopný / neschopný interakce(tedy vést nějaký rozhovor, interagovat s předměty inventáře)
					
			tedy postava/objekt může být jakoukoliv kombinací předchozích tedy 2x2x2=8 možností, tedy třeba neviditelný pro hráče, viditelný pro myš, ale nechopný interakce, jen aby hráče nasral
      
      pokud postava/objekt reaguje pouze na nejaky predmet, je interaktivni, pouze postava hrace pronasi neco "nebbudu prudit"

			TODO 
			aktiv
				isVisible
				mouseInteractivityShown
				isInteractive
			inactive
				!isVisible
				!mouseInteractivityShown
				!isInteractive
			makePresent

	location
		gain = hráč je informován o tom, že objevil lokaci
		loose = hráč je informován o tom, že zapoměl kde je lokace
		isOpened = hráč není informován o tom, že má nově lokaci na mapě /hráč není informován o tom, že přišel lokaci na mapě
		isOnMap = hráč není informován o tom, že se na lokaci dá dostat pomocí přechodových šipek



		isOpened/isOnMap
					- isOpened=1 
					  isOnMap=1 jde navstivit pres mapu i prechodove sipky
  				- isOpened=1 
					  isOnMap=0 jde navstivit jen pres prechodove sipky
  				- isOpened=0 
					  isOnMap=1 asi mit pripraveny hlasky jako ze nevi jak se tam de, nebo ze tam nechce


